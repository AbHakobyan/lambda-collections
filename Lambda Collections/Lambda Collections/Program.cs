﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lambda_Collections
{
    class Program
    {
        static void Main(string[] args)
        {
            //Lyambda Method
            //MFunc mFunc2 = new MFunc(
            //    delegate (Person p) 
            //    {
            //        return p.Name.StartsWith("V");
            //    });

            //Lyambda Operator
            //MFunc mFunc3 = p => { return p.Age == 1; };

            //Lyambda Expression
            //MFunc mFunc4 = p => p.Age == 1;

            var workers = CreateWorker(50).ToList();

            List<Workers> work = workers.Serching(w => w.Surename.StartsWith("Mu")).ToList();


            foreach (var item in work)
            {
                Console.WriteLine(item);

            }

        }

        static IEnumerable<Workers> CreateWorker(int count)
        {
            var name = new string[]
            {
                "Aram",
                "Ashot",
                "Vardan",
                "Arman",
                "Gevorg"
            };

            var surename = new string[]
            {
                "Mheryan",
                "Muradyan",
                "Hovhanisyan",
                "Amiryan"
            };

            var rnd = new Random();
            for (int i = 0; i < count; i++)
            {
                var index = rnd.Next(0, name.Length);
                var index2 = rnd.Next(0, surename.Length);

                var workers = new Workers(name[index], surename[index2], rnd.Next(20, 50));
                yield return workers;
            }

        }
    }

    static class EnumerableExtension
    {
        public static IEnumerable<T> Serching<T>(this IEnumerable<T> source, Func<T, bool> condition)
        {
            foreach (var item in source)
            {
                if (condition(item))
                {
                    yield return item;
                }
            }
        }
    }

    class Workers
    {
        public string Name { get; set; }
        public string Surename { get; set; }
        public int Age { get; set; }

        public Workers(string value, string item, int temp)
        {
            Name = value;
            Surename = item;
            Age = temp;
        }

        public override string ToString()
        {
            return $"{Name}  {Surename} - {Age}";
        }
    }
}
